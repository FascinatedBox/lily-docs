@title: Docgen

### Introduction

Docgen is Lily's documentation generator. It's used to generate the
documentation for Lily's prelude as well as libraries written in Lily.

To generate documentation for a foreign library, run docgen with the path to the
manifest file. For native libraries, run docgen to the file that will be
imported.

Docgen works by creating a subinterpreter and running the file provided.
Afterward, introspection is used to discover symbols and their docblocks. This
approach allows docgen to work identically for native and foreign libraries.

Docgen generates documentation for symbols that are visible from the first file
provided. This includes both symbols that are declared in the module as well as
symbols it has imported.

### Download

You will need to have Lily installed, because coretools (bindgen and docgen) use
a C library (spawni) that links to Lily's library. The easiest way to get them
is to download garden (the package manager) and have it install coretools.
Garden is available here:

[https://gitlab.com/FascinatedBox/lily-garden](https://gitlab.com/FascinatedBox/lily-garden)

Garden does not have any dependencies. Execute `run_garden.lily` with
`install FascinatedBox/lily-coretools` to get and prepare coretools.

Bindgen and docgen are located in the `src` directory of coretools.

### Output

Docgen generates output on a per-symbol basis. Here is what various symbols will
result in.

Module pages list all classes and enums inside. Toplevel functions and variables
are listed in a module's page.

Class pages list their **protected** and **public** methods. Native classes will
also list their properties.

Enum pages list their variants and their methods.

### Features

Docgen supports a limited set of markdown for writing documentation.

```
Lines of text that are next to each other
like this
will be merged into one paragraph.
Use an empty line to start a new paragraph.

Using backticks to highlight symbol names is recommended.

* Bulleted lists are supported.

Fenced code blocks work as well.
Using \`\`\`lily as the header is recommended.

Only two section headers are allowed:

# Examples

and

# Errors
```

If the file passed to docgen successfully parses, docgen will try to render all
symbols that are visible. If docgen finds any error, it will still process all
symbols to look for additional errors. At the end of processing, it will either
report all errors and exit unsuccessfully, or generate documentation.

By default, docgen generates documentation to a `doc` directory. However, the
directory can be customized.
