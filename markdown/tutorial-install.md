@title: Install

### Building

To build the Lily interpreter, you will need the following:

* CMake at version 3.0 or higher.

* A C compiler that supports C11. Most any modern C compiler will work.

After cloning Lily and entering the directory, execute the following.

```
cmake .
cmake --build .
```

The above two commands work on all platforms. On some platforms, you may also be
able to use `make install` to install Lily.

Successfully building Lily results in a `lily` executable in the toplevel
directory.

### Modes

The interpreter has three ways of reading content.

Code mode is the default. In code mode, all content is read as code and executed
on success. Lily tags are a syntax error in this mode.

In template mode, code is between `<?lily ... ?>` tags, and content outside of
those tags is rendered. Additionally, it is a syntax error if the file does not
begin with a Lily tag, even if the tag is empty. This restriction exists to
prevent template mode from accidentally "rendering" a code file.

Imports done from a template always import in code mode, allowing templates and
code files to share code.

The third mode is manifest mode. Manifest mode is used to specify symbols that a
C library will export. More information is available in the `bindgen` section.
Reading through the tutorial beforehand is highly recommended.

### Usage

Create a file named `hello.lily` and write the following.

```
print("Hello, world!")
```

Execute the script with `lily hello.lily`. You should see the message on your
console.

The interpreter can also be invoked from the console as follows:

```
lily -s 'print("Hello, world!")`
```

By default, Lily reads scripts in code mode. The interpreter also supports a
template mode that is available using `-t`.

Create a file named `template.lily` and write the following.

```
<?lily ?>
<html>
    <body>
        <?lily print("<p>Hello World</p>") ?>
    </body>
</html>
```

Execute the script with `lily -t template.lily`. You should see the content
between `<?lily ... ?>` tags rendered.
