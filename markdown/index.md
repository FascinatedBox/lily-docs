@title: Welcome

Lily is a programming language that's been under development for
several years. Lily is **statically-typed**, with an interpreter as a reference.
Lily uses **reference counting** for memory management with **garbage
collection** as a fallback.

Key features of Lily:

* Built-in template mode
* Embed/extend in C
* Single-inheritance classes
* Exceptions
* Generics
* Algebraic data types (with `Option` and `Result` predefined).

Here's a small example showing most of Lily's features:

```lily
var math_ops = ["+" => (|a: Integer, b: Integer| a + b),
                "-" => (|a, b| a - b),
                "*" => (|a, b| a * b),
                "/" => (|a, b| a / b)]

define rpn(input: String): Result[String, List[Integer]]
{
    var values = input.split(" ")
    var stack: List[Integer] = []

    foreach v in values: {
        with v.parse_i() as Some(s): {
            s |> stack.push
        else:
            if stack.size() < 2: {
                return Failure("Stack underflow.")
            }

            var right = stack.pop()
            var left = stack.pop()
            try: {
                var op_fn = math_ops[v]
                var op_value = op_fn(left, right)
                stack.push(op_value)
            except KeyError:
                return Failure("Invalid operation {}.".format(v))
            except DivisionByZeroError:
                return Failure("Attempt to divide by zero.")
            except Exception:
                return Failure("Unexpected error from op {}.".format(v))
            }
        }
    }

    return Success(stack)
}

print("1 2 3 4 * + -"  |> rpn) # Success([-13])
print("2 2 2 2 * *"    |> rpn) # Success([2, 8])
print("*"              |> rpn) # Failure("Stack underflow.")
print("1 2 ?"          |> rpn) # Failure("Invalid operation ?.")
```
