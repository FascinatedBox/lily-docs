@title: Exceptions

Occasionally an error occurs that is not expected:: A subscript of a `List` is
out-of-bounds. An attempt to divide by zero. A `Hash` is missing a key.

In many cases, the predefined `Option` and `Result` enums are preferable to
raising an exception. However, error classes exist for cases where `Option` and
`Result` are not suitable.

The base class of all errors is `Exception`. Only classes that inherit from
`Exception` can be raised. The `Exception` class include two members: a
`String` message, and a `List[String]` traceback.

When an exception is raised, the `traceback` field of it is replaced with the
current call stack.

Information about exception capture is available in the `try` section of blocks
located [here](tutorial-blocks.html#try).

### Hierarchy

The exception hierarchy is as follows:

* `DivisionByZero`: Attempt to divide or modulo by zero.

* `Exception`: Base class of all raiseable exceptions.

* `IndexError`: Out-of-bounds access of a container (such as `List`).

* `IOError`: Incorrect usage of a `File`.

* `KeyError`: Attempt to read a value from a `Hash` that does not exist.

* `RuntimeError`: Bad runtime action such as modifying a hash during iteration
                  or exceeding the recursion limit.

* `ValueError`: Invalid or unreasonable value provided.

Any of these exceptions can be inherited to create a custom exception. The try
section in the blocks area details how `try/except` works, and more on raising
custom exceptions.
